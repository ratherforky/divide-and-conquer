{-# language DeriveFunctor, RecordWildCards #-}

module MergeSort where

import Control.Arrow ((>>>), (&&&), first, second)
import Data.Foldable

-- Inspired by https://retro-freedom.nz/haskell-magic-hylomorphisms-and-divide-and-conquer.html


type Algebra f a = f a -> a
type Coalgebra f a = a -> f a

newtype Term f = In { out :: f (Term f) }

cata :: (Functor f) => Algebra f a -> Term f -> a
cata f = out >>> fmap (cata f) >>> f

ana :: (Functor f) => Coalgebra f a -> a -> Term f
ana f = f >>> fmap (ana f) >>> In

hylo :: (Functor f) => Coalgebra f a -> Algebra f b -> a -> b
hylo f g = ana f >>> cata g

-- Actual merge sort part


data WorkTreeF a s = Leaf a | Bin s s
  deriving (Functor)

type WorkTree a = Term (WorkTreeF a)

mergeSort :: Ord a => [a] -> [a]
mergeSort = hylo divide conquer

divide :: Coalgebra (WorkTreeF [a]) [a]
divide xs = case xs of
  []  -> Leaf []
  [_] -> Leaf xs
  _   -> let half = length xs `div` 2 in
      Bin (take half xs) (drop half xs)

conquer :: Ord a => Algebra (WorkTreeF [a]) [a]
conquer (Leaf xs)   = xs
conquer (Bin xs ys) = merge xs ys

merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) | x <= y    = x:merge xs (y:ys)
                    | otherwise = y:merge (x:xs) ys

-- Abstracting divide and conquer

divNconq :: (b -> b -> b) -> ([a] -> Maybe b) -> [a] -> b
divNconq combine trivial = hylo divide' conquer'
  where
  --divide' :: Coalgebra (WorkTreeF [a]) b
    divide' xs = case trivial xs of
      Just b  -> Leaf b
      Nothing -> Bin (take half xs) (drop half xs)
        where half = length xs `div` 2

    -- conquer' :: Algebra (WorkTreeF [a]) b
    conquer' (Leaf b)  = b
    conquer' (Bin x y) = combine x y

divNconqSemi :: Semigroup b => ([a] -> Maybe b) -> [a] -> b
divNconqSemi trivial = hylo divide' conquer'
  where
  --divide' :: Coalgebra (WorkTreeF [a]) b
    divide' xs = case trivial xs of
      Just b  -> Leaf b
      Nothing -> Bin (take half xs) (drop half xs)
        where half = length xs `div` 2

    -- conquer' :: Algebra (WorkTreeF [a]) b
    conquer' (Leaf b)  = b
    conquer' (Bin x y) = x <> y

-- Merge sort again

mergeSort' :: Ord a => [a] -> [a]
mergeSort' = divNconq merge trivial
  where
    -- Define trivial cases
    -- Just with trivial result
    -- Nothing if non-trivial
    trivial []  = Just []
    trivial [x] = Just [x]
    trivial _   = Nothing

maxSubArray :: (Num a, Ord a) => [a] -> (a, [a])
maxSubArray = (\(maxVal, maxList, _) -> (maxVal, maxList)) 
            . divNconq conq trivial
  where
    trivial [] = Just (0, [], [])
    trivial [x]
      | x >= 0    = Just (x, [x], [x])
      | otherwise = Just (0, [], [x])
    trivial _ = Nothing

    conq (maxX, maxSubXs, xs) (maxY, maxSubYs, ys) = (maxBoth, maxSubArray, xs ++ ys)
      where
        (maxBoth, maxSubArray) = maximumOn fst [(maxX, maxSubXs), (maxY, maxSubYs), maxSubCrossover]

        maxSubCrossover = (\(x, xs) (y, ys) -> (x + y, xs ++ reverse ys))
                            (maxSubArrayFromEnd xs)
                            (maxSubArrayFromEnd (reverse ys))

maxSubArraySemi :: (Num a, Ord a) => [a] -> (a, [a])
maxSubArraySemi = (\(CS maxVal maxList _) -> (maxVal, maxList)) 
                . divNconqSemi trivial
  where
    trivial [] = Just (CS 0 [] [])
    trivial [x]
      | x >= 0    = Just (CS x [x] [x])
      | otherwise = Just (CS 0 []  [x])
    trivial _ = Nothing

data ConqState a = CS a [a] [a]

instance (Num a, Ord a) => Semigroup (ConqState a) where
  (CS maxX maxSubXs xs) <> (CS maxY maxSubYs ys) = CS maxBoth maxSubArray (xs ++ ys)
    where
      (maxBoth, maxSubArray) = maximumOn fst [(maxX, maxSubXs), (maxY, maxSubYs), maxSubCrossover]

      maxSubCrossover = (\(x, xs) (y, ys) -> (x + y, xs ++ reverse ys))
                          (maxSubArrayFromEnd xs)
                          (maxSubArrayFromEnd (reverse ys))

data State a = State
  { bestSum  :: a
  , bestList :: [a]
  , currSum  :: a
  , currList :: [a]
  }

maxSubArrayFromEnd :: (Num a, Ord a) => [a] -> (a, [a])
maxSubArrayFromEnd = foldr f (State 0 [] 0 []) 
                 >>> bestSum &&& bestList
  where
    f :: (Num a, Ord a) => a -> State a -> State a
    f x State{..} -- RecordWildCard, puts all record data in scope with corresponding name
      = State
        { bestSum  = bestSum'
        , bestList = bestList'
        , currSum  = currSum'
        , currList = currList'
        }
      where
        (currSum', currList') = (currSum + x, x : currList)
        (bestSum', bestList') = maxOn fst (currSum', currList') (bestSum, bestList)


maximumOn :: Ord b => (a -> b) -> [a] -> a
maximumOn f = foldr1 (maxOn f) 

maxOn :: Ord b => (a -> b) -> a -> a -> a
maxOn f x y
  | f x >= f y = x
  | otherwise  = y